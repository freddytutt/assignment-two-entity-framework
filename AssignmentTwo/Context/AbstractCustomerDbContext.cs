﻿using AssignmentTwo.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTwo.Context
{
    /// <summary>
    /// The sole purpose of this adapter is to allow DbContexts with a pre-defined customer model DbSet.
    /// </summary>
    public abstract class AbstractCustomerDbContext: DbContext, ICustomerModelDbSetProperty, IGenreModelDbSetProperty
    {
        /// <summary>
        /// Now the Customers are accessible through this adapter, together with all of DbContext's functionality.
        /// </summary>
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Genre> Genres { get; set; }
        protected AbstractCustomerDbContext()
        {
        }
        /// <summary>
        /// Just pass on the construction to DbContext.
        /// </summary>
        /// <param name="options"></param>
        protected AbstractCustomerDbContext(DbContextOptions options) : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=N-NO-01-04-8024\\SQLEXPRESS;Initial Catalog=chinook; Integrated Security=True;Encrypt=True;TrustServerCertificate=True");

    }
}
