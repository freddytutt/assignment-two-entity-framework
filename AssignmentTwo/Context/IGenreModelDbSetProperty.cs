﻿using AssignmentTwo.Model;
using Microsoft.EntityFrameworkCore;

namespace AssignmentTwo.Context
{
    /// <summary>
    /// A DbSet property to be used with the adapter (in this case the customer model).
    /// </summary>
    public interface IGenreModelDbSetProperty
    {
        public DbSet<Genre> Genres { get; set; }
    }
}
