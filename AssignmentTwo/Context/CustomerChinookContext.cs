﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AssignmentTwo.Context
{

    /// <summary>
    /// A subset of the ChinookContext (with customer dbset property only).
    /// </summary>
    public class CustomerChinookContext : AbstractCustomerDbContext
    {
        public CustomerChinookContext()
        {
        }

        public CustomerChinookContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=N-NO-01-04-8024\\SQLEXPRESS;Initial Catalog=chinook; Integrated Security=True;Encrypt=True;TrustServerCertificate=True");

    }
}
