﻿namespace AssignmentTwo.ViewModel
{
    public static class CustomerExtensions
    {
        public static ViewModel.Customer AsViewModel(this Model.Customer customer)
        {
            return new ViewModel.Customer(customer);
        }

    }
}
