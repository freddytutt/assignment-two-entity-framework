﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTwo.ViewModel
{
    public class Customer
    {
        public Customer() { }
        public Customer(Model.Customer mc) : this(mc.CustomerId, mc.FirstName, mc.LastName, mc.Country, mc.PostalCode, mc.Phone, mc.Email)
        {

        }
        public Customer(ViewModel.Customer mc) : this(mc.CustomerId, mc.FirstName, mc.LastName, mc.Country, mc.PostalCode, mc.Phone, mc.Email)
        {

        }
        public Customer(int customerId, string firstName, string lastName, string? country, string? postalCode, string? phone, string email)
        {
            CustomerId = customerId;
            FirstName = firstName;
            LastName = lastName;
            Country = country;
            PostalCode = postalCode;
            Phone = phone;
            Email = email;
        }

        public int CustomerId { get; set; }

        public string FirstName { get; set; } = null!;

        public string LastName { get; set; } = null!;
        public string? Country { get; set; }

        public string? PostalCode { get; set; }

        public string? Phone { get; set; }

        public string Email { get; set; } = null!;
}
}
