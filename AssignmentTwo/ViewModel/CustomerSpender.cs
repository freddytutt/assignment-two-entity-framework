﻿using AssignmentTwo.Context;
using AssignmentTwo.Model;
using AssignmentTwo.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AssignmentTwo.ViewModel
{
    /// <summary>
    /// Class for
    /// </summary>
    public class CustomerSpender : ViewModel.Customer
    {
        public CustomerSpender(ViewModel.Customer customer, decimal total):base(customer) {
            Total = total;
        }
        public decimal Total { get; }
    }
}
