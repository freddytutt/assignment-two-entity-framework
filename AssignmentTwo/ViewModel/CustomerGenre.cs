﻿using AssignmentTwo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTwo.ViewModel
{
    public class CustomerGenre : ViewModel.Customer
    {
        public List<Genre> FavoriteGenres { get; }
        public CustomerGenre(Customer customer, List<Genre> favoriteGenres) : base(customer) {
            FavoriteGenres = favoriteGenres;
        }
    }
}
