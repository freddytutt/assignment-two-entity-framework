﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTwo.ViewModel
{
    /// <summary>
    /// Wrapping
    /// </summary>
    public class CustomerCountry
    {
        public CustomerCountry(IEnumerable<Customer> customers, string? country)
        {
            Customers = customers;
            Country = country;
        }
        public IEnumerable<Customer> Customers { get; set; }
        public string? Country { get; set; }
    }
}
