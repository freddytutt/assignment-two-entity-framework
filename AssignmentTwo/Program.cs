﻿using assignment_two.Data_Access;
using AssignmentTwo.Repository;
using AssignmentTwo.Model;
using AssignmentTwo.Context;

namespace AssignmentTwo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository dbCustomerRepository = new ChinookCustomerRepository();
            Model.Customer customer = new Model.Customer { FirstName="albert",LastName="åberg",Email="not"};
            Console.WriteLine(dbCustomerRepository.GetAll());
            Console.WriteLine(dbCustomerRepository.Add(customer));
            customer.LastName = "Strand";
            Console.WriteLine(dbCustomerRepository.Update(customer));
            Console.WriteLine(dbCustomerRepository.Get(customer.CustomerId));
            Console.WriteLine(dbCustomerRepository.Delete(customer.CustomerId));
        }
    }
}