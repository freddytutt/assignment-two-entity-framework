﻿using assignment_two.Data_Access;
using AssignmentTwo.Context;
using AssignmentTwo.Model; 
using AssignmentTwo.ViewModel; 
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTwo.Repository
{
    /// <summary>
    /// This class is created for better readability.
    /// </summary>
    public interface ICustomerRepository : IRepository<Model.Customer>
    {
        public IEnumerable<CustomerSpender> GetHighestSpenders();
        public IEnumerable<CustomerCountry> GetCustomerCountries();
        public CustomerGenre GetCustomerGenre(int ID);
    }

}
