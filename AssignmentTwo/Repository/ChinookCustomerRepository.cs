﻿using assignment_two.Data_Access;
using AssignmentTwo.Context;
using AssignmentTwo.Model;
using AssignmentTwo.ViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentTwo.Repository
{
    public class ChinookCustomerRepository : AbstractCustomerRepository
    {
        private CustomerChinookContext _customerChinookContext { get; set; }
        protected override AbstractCustomerDbContext DbContext
        {
            get { return _customerChinookContext ?? (_customerChinookContext = new CustomerChinookContext()); }
            set { _customerChinookContext = _customerChinookContext == null ? new CustomerChinookContext() : (CustomerChinookContext)value; }
        }
        public ChinookCustomerRepository() => DbContext = new CustomerChinookContext();
        public ChinookCustomerRepository(AbstractCustomerDbContext dbContext) : base(dbContext)
        {
        }


        public override IEnumerable<Model.Customer> GetAll()
        {
            try
            {
                return DbContext.Customers;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Something went wrong when retrieving all customers.");
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
                return new List<Model.Customer>();
            }
        }
        public override Model.Customer Get(int ID)
        {
            try
            {
                return DbContext.Customers.Find(ID);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.ToString());
                return new Model.Customer() { };
            }
        }

        public override bool Add(Model.Customer customer)
        {
            try
            {
                DbContext.Customers.Add(customer);
                DbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.ToString());
                return false;
            }
            return true;
        }

        public override bool Delete(int ID)
        {
            try
            {
                Model.Customer customerToDelete = DbContext.Customers.Find(ID);
                DbContext.Customers.Remove(customerToDelete);
                DbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.ToString());
                return false;
            }
            return true;
        }



        public override bool Update(Model.Customer customer)
        {
            try
            {
                Model.Customer customerToUpdate = DbContext.Customers.Find(customer.CustomerId);
                customerToUpdate = customer;
                DbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.ToString());
                return false;
            }
            return true;
        }

        protected override AbstractCustomerDbContext GetDefaultContext()
        {
            return new CustomerChinookContext();
        }

        public override IEnumerable<CustomerSpender> GetHighestSpenders()
        {
            List<Model.Customer> highestSpendingCustomers = DbContext.Customers.OrderByDescending(c=>c.Invoices.Sum(invoice=>invoice.Total)).ToList();
            List<CustomerSpender> customerSpenders = new List<CustomerSpender>();
            return highestSpendingCustomers.Select((c)=>new CustomerSpender(c.AsViewModel(), c.Invoices.Sum(i=>i.Total)));
        }

        public override IEnumerable<CustomerCountry> GetCustomerCountries()
        {
            throw new NotImplementedException(); 
        }

        public override CustomerGenre GetCustomerGenre(int ID)
        {
            List<Genre> favoriteGenres = new List<Genre>();

            var customer = DbContext.Customers.First(customer=>customer.CustomerId== ID);
            var customerView = customer.AsViewModel();
            var invoices = customer.Invoices.ToList();

            IEnumerable<Track> tracks = new List<Track>();
            List<string> genreApperances = new List<string>();
            invoices.ForEach(invoice=>invoice.InvoiceLines.ToList().ForEach(line=> genreApperances.Add(line.Track.Genre.Name)));

            favoriteGenres = favoriteGenres.GroupBy(genre => genre).OrderByDescending(grp => grp.Count()).Select(grp => grp.Key).ToList();

            return new CustomerGenre(customerView, favoriteGenres);
        }
    }
}
