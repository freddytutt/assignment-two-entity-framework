﻿using AssignmentTwo.Context;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment_two.Data_Access
{
    /// <summary>
    /// Repository interface defining common repository methods.
    /// </summary>
    /// <typeparam name="T">Type of the contained value(s).</typeparam>
    public interface IRepository<T> where T : class, new()
    {
        /// <summary>
        /// Gets all records from repository.
        /// </summary>
        /// <returns>A generic collection of records.</returns>
        public IEnumerable<T> GetAll();

        /// <summary>
        /// Adds record to repository.
        /// </summary>
        /// <param name="record">The record to be added.</param>
        /// <returns>A boolean value indicating success.</returns>
        public bool Add(T record);

        /// <summary>
        /// Gets single record by ID.
        /// </summary>
        /// <param name="ID">The ID of the retrieving record.</param>
        /// <returns>A single record.</returns>
        public T Get(int ID);

        /// <summary>
        /// Updates a record by looking at the records ID.
        /// </summary>
        /// <param name="record">The updating record.</param>
        /// <returns>A boolean value indicating success.</returns>
        public bool Update(T record);

        /// <summary>
        /// Deletes a record by ID.
        /// </summary>
        /// <param name="ID">The ID of the deleting record.</param>
        /// <returns>A boolean value indicating success.</returns>
        public bool Delete(int ID);

    }
}
