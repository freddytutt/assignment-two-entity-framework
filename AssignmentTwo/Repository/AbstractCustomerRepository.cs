﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssignmentTwo.Context;
using AssignmentTwo.ViewModel;
using Microsoft.EntityFrameworkCore;

namespace AssignmentTwo.Repository
{
    public abstract class AbstractCustomerRepository : ICustomerRepository 
    {
        protected abstract AbstractCustomerDbContext DbContext { get; set; }
        public AbstractCustomerRepository()
        {
            DbContext = GetDefaultContext();
        }
        public AbstractCustomerRepository(AbstractCustomerDbContext dbContext)
        {
            DbContext = dbContext;
        }
        protected abstract AbstractCustomerDbContext GetDefaultContext();
        public abstract IEnumerable<Model.Customer> GetAll();
        public abstract bool Add(Model.Customer record);
        public abstract Model.Customer Get(int ID);
        public abstract bool Update(Model.Customer record);
        public abstract bool Delete(int ID);
        public abstract IEnumerable<CustomerSpender> GetHighestSpenders();
        public abstract IEnumerable<CustomerCountry> GetCustomerCountries();
        public abstract CustomerGenre GetCustomerGenre(int ID);
    }
}
